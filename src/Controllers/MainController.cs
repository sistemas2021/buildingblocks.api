﻿using System.Linq;
using BuildingBlocks.Core.src.Interfaces;
using BuildingBlocks.Core.src.Notificacoes;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace BuildingBlocks.Api.src.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public abstract class MainController : ControllerBase
    {
        private readonly INotificador _notificador;

        protected MainController(INotificador notificador)
        {
            _notificador = notificador;
        }

        protected ActionResult CustomResponse(object result = null)
        {
            if (OperacaoValida())
            {
                return Ok(new
                {
                    success = true,
                    data = result
                });
            }

            return BadRequest(new
            {
                success = false,
                errors = _notificador.ObterNotificacoes().Select(n => n.Mensagem)
            });            
        }

        protected ActionResult CustomResponse(ModelStateDictionary modelState)
        {
            var erros = modelState.Values.SelectMany(e => e.Errors);
            foreach (var erro in erros)
            {
                NotificarErro(erro.ErrorMessage);
            }

            return CustomResponse();
        }

        protected ActionResult CustomResponse(ValidationResult validationResult)
        {
            foreach (var erro in validationResult.Errors)
            {
                NotificarErro(erro.ErrorMessage);
            }

            return CustomResponse();
        }

        // protected ActionResult CustomResponse(ResponseResult resposta)
        // {
        //     ResponsePossuiErros(resposta);

        //     return CustomResponse();
        // }

        // protected bool ResponsePossuiErros(ResponseResult resposta)
        // {
        //     if (resposta == null || !resposta.Errors.Mensagens.Any()) return false;

        //     foreach (var mensagem in resposta.Errors.Mensagens)
        //     {
        //         NotificarErro(mensagem);
        //     }

        //     return true;
        // }

        protected bool OperacaoValida()
        {
            return !_notificador.TemNotificacao();
        }

        protected void NotificarErro(string mensagem)
        {
            _notificador.Handle(new Notificacao(mensagem));
        }
    }
}