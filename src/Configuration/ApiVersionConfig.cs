using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace BuildingBlocks.Api.src.Configuration
{
    public static class ApiVersionConfig
    {
        public static IServiceCollection AddApiVersionConfiguration(this IServiceCollection services)
        {
            services.AddApiVersioning(p =>
            {
                p.ReportApiVersions = true;
                p.DefaultApiVersion = new ApiVersion(1, 0);
                p.AssumeDefaultVersionWhenUnspecified = true;
            });

            services.AddVersionedApiExplorer(p =>
            {
                p.GroupNameFormat = "'v'VVV";
                p.SubstituteApiVersionInUrl = true;
            });

            return services;
        }

        public static IApplicationBuilder UseApiVersionConfiguration(this IApplicationBuilder app)
        {
            app.UseApiVersioning();

            return app;
        }
    }
}