﻿namespace BuildingBlocks.Api.src.Identidade
{ 
    public class AppSettings
    {
        public string AutenticacaoJwksUrl { get; set; }
        public string Issuer { get; set; }
    }
}