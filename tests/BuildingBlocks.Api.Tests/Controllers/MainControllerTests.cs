using System.Collections.Generic;
using System.Linq;
using BuildingBlocks.Api.src.Controllers;
using BuildingBlocks.Core.src.Interfaces;
using BuildingBlocks.Core.src.Notificacoes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Moq;
using Moq.AutoMock;
using Xunit;

namespace BuildingBlocks.Api.Tests.Controllers
{
    public class TestController : MainController 
    {
        public TestController(INotificador notificador) : base(notificador)
        {
        }
        public new ActionResult CustomResponse(object result = null) => base.CustomResponse(result);
        public new ActionResult CustomResponse(ModelStateDictionary modelState) => base.CustomResponse(modelState);
        public new void NotificarErro(string mensagem) => base.NotificarErro(mensagem);
        public new bool OperacaoValida() => base.OperacaoValida();
    }
    public class MainControllerTests
    {
        private readonly AutoMocker _mocker;
        public MainControllerTests()
        {
            _mocker = new AutoMocker();
        }        

        [Fact(DisplayName = "Notificar erro controller")]
        [Trait("Categoria","Controller")]
        public void Controller_NotificarErro_DeveExecutarComSucesso()
        {
            // Arrange
            var testController = _mocker.CreateInstance<TestController>();
        
            // Act
            testController.NotificarErro("");
        
            // Assert
            _mocker.GetMock<INotificador>().Verify(r=>r.Handle(It.IsAny<Notificacao>()), Times.Once);
        }

        [Fact(DisplayName = "Operacao válida controller")]
        [Trait("Categoria","Controller")]
        public void Controller_OperacaoValida_DeveExecutarComSucesso()
        {
            // Arrange
            var testController = _mocker.CreateInstance<TestController>();
            _mocker.GetMock<INotificador>().Setup(r => r.TemNotificacao()).Returns(false);

            // Act
            var resultado = testController.OperacaoValida();
        
            // Assert
            Assert.True(resultado);
            _mocker.GetMock<INotificador>().Verify(r=>r.TemNotificacao(), Times.Once);
        }

        [Fact(DisplayName = "CustomResponse(object result = null) status 200")]
        [Trait("Categoria","Controller")]
        public void Controller_CustomResponse_DeveExecutarComSucesso()
        {
            // Arrange
            var testController = _mocker.CreateInstance<TestController>();
            _mocker.GetMock<INotificador>().Setup(r => r.TemNotificacao()).Returns(false);

            // Act
            var resultado = testController.CustomResponse();

            // Assert
            Assert.IsType<OkObjectResult>(resultado);
            _mocker.GetMock<INotificador>().Verify(r=>r.TemNotificacao(), Times.Once);
            _mocker.GetMock<INotificador>().Verify(r=>r.Handle(It.IsAny<Notificacao>()), Times.Never);
        }

        [Fact(DisplayName = "CustomResponse(object result = null) status 400")]
        [Trait("Categoria","Controller")]
        public void Controller_CustomResponse_DeveExecutarComErro()
        {
            // Arrange
            var mensagem = new List<Notificacao>(){new Notificacao("Erro no processamento")};
            var testController = _mocker.CreateInstance<TestController>();
            _mocker.GetMock<INotificador>().Setup(r => r.TemNotificacao()).Returns(true);
            _mocker.GetMock<INotificador>().Setup(r => r.ObterNotificacoes()).Returns(mensagem);

            // Act
            var resultado = testController.CustomResponse();

            // Assert
            Assert.IsType<BadRequestObjectResult>(resultado);
            _mocker.GetMock<INotificador>().Verify(r=>r.TemNotificacao(), Times.Once);
            _mocker.GetMock<INotificador>().Verify(r=>r.Handle(It.IsAny<Notificacao>()), Times.Never);
        }

        [Fact(DisplayName = "CustomResponse(ModelStateDictionary modelState) status 200")]
        [Trait("Categoria","Controller")]
        public void Controller_CustomResponseModelState_DeveExecutarComSucesso()
        {
            // Arrange
            var testController = _mocker.CreateInstance<TestController>();
            _mocker.GetMock<INotificador>().Setup(r => r.TemNotificacao()).Returns(false);
            var modelState = new ModelStateDictionary();

            // Act
            var resultado = testController.CustomResponse(modelState);

            // Assert
            Assert.IsType<OkObjectResult>(resultado);
            _mocker.GetMock<INotificador>().Verify(r=>r.TemNotificacao(), Times.Once);
            _mocker.GetMock<INotificador>().Verify(r=>r.Handle(It.IsAny<Notificacao>()), Times.Never);
        }

        [Fact(DisplayName = "CustomResponse(ModelStateDictionary modelState) status 400")]
        [Trait("Categoria","Controller")]
        public void Controller_CustomResponseModelState_DeveExecutarComErro()
        {
            // Arrange
            var mensagem = new List<Notificacao>(){new Notificacao("Erro no processamento")};
            var testController = _mocker.CreateInstance<TestController>();
            _mocker.GetMock<INotificador>().Setup(r => r.TemNotificacao()).Returns(true);
            _mocker.GetMock<INotificador>().Setup(r => r.ObterNotificacoes()).Returns(mensagem);
            var modelState = new ModelStateDictionary();
            modelState.AddModelError("","");

            // Act
            var resultado = testController.CustomResponse(modelState);

            // Assert
            Assert.IsType<BadRequestObjectResult>(resultado);
            _mocker.GetMock<INotificador>().Verify(r=>r.TemNotificacao(), Times.Once);
            _mocker.GetMock<INotificador>().Verify(r=>r.Handle(It.IsAny<Notificacao>()), Times.Once);
        }
    }
}
